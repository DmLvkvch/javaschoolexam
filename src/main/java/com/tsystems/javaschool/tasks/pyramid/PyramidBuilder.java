package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(inputNumbers.contains (null)){
            throw new CannotBuildPyramidException ();
        }
        double rows = (-1+Math.sqrt (1+8*inputNumbers.size ()))/2;
        int n = 0, m = 0;
        if(Math.abs (rows - (int)rows)<0.00001){
            n = (int)rows;
        }
        else {
            throw new CannotBuildPyramidException ();
        }
        Collections.sort (inputNumbers);
        m = n*2-1;
        int[][] pyramid = new int[n][m];
        int pos = (int)Math.ceil ((double)m/2)-1;
        int step = pos;
        Iterator<Integer> number = inputNumbers.iterator ();
        for (int i = 0; i < n; i++) {
            for (int j = 0;j<=i;j++) {
                pyramid[i][step] = number.next ();
                step+=2;
            }
            pos--;
            step = pos;
        }
        return pyramid;
    }


}
