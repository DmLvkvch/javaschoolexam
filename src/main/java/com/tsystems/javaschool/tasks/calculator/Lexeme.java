package com.tsystems.javaschool.tasks.calculator;

enum Lexeme {
    CloseBracket,
    Number,
    OpenBracket,
    Operator,
    Unknown
}