package com.tsystems.javaschool.tasks.calculator;

import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement){
        if(statement==null || statement.equals ("")){
            return null;
        }
        List<Token> tokens = Parser.tokenizer (statement);
        Stack<Token> operands = new Stack<> ();
        Stack<Token> operators = new Stack<> ();
        for (Token token:
             tokens) {
            if(token.getLexeme ()== Lexeme.Number){
                operands.push (token);
            }
            else if(token.getLexeme () == Lexeme.Operator || token.getLexeme () == Lexeme.OpenBracket){
                if(token.getLexeme ()== Lexeme.OpenBracket) {
                    operators.push (token);
                }
                else{
                    if (operators.empty ( ) || (token.getPriority ( ) < operators.peek ( ).getPriority ( )))
                        operators.push (token);
                    else {
                        try {
                            evaluate (operands, operators);
                        } catch (Exception e) {
                            return null;
                        }
                        operators.push (token);
                    }
                }
            }
            else if(token.getLexeme () == Lexeme.CloseBracket){
                while(operators.size ()>0 && operators.peek ().getLexeme () != Lexeme.OpenBracket){
                    try {
                        evaluate (operands, operators);
                    }
                    catch (Exception ex){
                        return null;
                    }
                }
                operators.pop ();
            }
        }
        while(!operators.empty ()){
            try {
                evaluate (operands, operators);
            } catch (Exception e) {
                return null;
            }
        }

        if (operands.size () > 1 || operators.size () > 0)
            return null;

        if(Math.abs (Float.parseFloat (operands.peek ().getValue ()) - (int)Float.parseFloat (operands.peek ().getValue ()))<0.00001){
            return Integer.toString ((int)Float.parseFloat (operands.peek ().getValue ()));
        }
        float result = Float.parseFloat (operands.pop ().getValue ())*10000;
        int roundedResult = Math.round(result);
        result = (float)roundedResult / 10000;
        return Float.toString (result);
    }

    private void evaluate(Stack<Token> operands, Stack<Token> operators) throws Exception {
        Token b = operands.pop ();
        Token a = operands.pop ();
        String operator = operators.pop ().getValue ();
        float firstNumber = Float.parseFloat (a.getValue ());
        float secondNumber = Float.parseFloat (b.getValue ());
        float result = 0;
        switch (operator) {
            case "+":
                result = firstNumber + secondNumber;
                break;
            case "*":
                result = firstNumber * secondNumber;
                break;
            case "-":
                result = firstNumber - secondNumber;
                break;
            case "/":
                if (Math.abs (secondNumber) < 0.00001)
                    throw new Exception ( );
                result = firstNumber / secondNumber;
                break;
        }
        operands.push (new Token (Float.toString (result)));
    }

}
