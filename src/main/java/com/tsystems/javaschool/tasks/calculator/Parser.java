package com.tsystems.javaschool.tasks.calculator;
import java.util.ArrayList;
import java.util.List;

class Parser {
    /**
     *
     * @param expression mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return list of tokens
     */
    static List<Token> tokenizer(String expression){
        expression = expression.replaceAll ("\\s","");
        List<Token> tokens = new ArrayList<> ();
        for (int i = 0; i <expression.length (); i++) {
            char c = expression.charAt (i);
            if(c == '+' || c == '-'
            || c =='/'|| c =='*' ||
                    c =='(' || c ==')'){
                Token token = new Token (c + "");
                tokens.add (token);
            }
            else if(Character.isDigit (c)){
                StringBuilder digit = new StringBuilder ();
                while((Character.isDigit (c) || c=='.') && i<expression.length ()-1){
                    digit.append (c);
                    i++;
                    c = expression.charAt (i);
                }
                if(i==expression.length ()-1 && Character.isDigit (c)){
                    digit.append (c);
                    i++;
                }
                i--;
                Token token = new Token (digit.toString ( ));
                tokens.add (token);
            }
        }
        return tokens;
    }
}
