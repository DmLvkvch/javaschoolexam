package com.tsystems.javaschool.tasks.calculator;

class Token {
    private String value;
    private int priority;
    private Lexeme lexeme;

    Token(String value){
        this.value = value;
        this.lexeme = determineToken (value);
        this.priority = getPriority (value);
    }

    private static int getPriority(String op) {
        switch (op) {
            case "(":
                return 3;
            case "*": case "/":
                return 1;
            case "+": case "-":
                return 2;
            default:
                return -2;
        }
    }

    int getPriority() {
        return priority;
    }

    private static Lexeme determineToken(String token){
        if(token.equals ("+") || token.equals ("-") || token.equals ("*") || token.equals ("/")){
            return Lexeme.Operator;
        }
        else if(token.equals ("(")){
            return Lexeme.OpenBracket;
        }
        else if(token.equals (")")){
            return Lexeme.CloseBracket;
        }
        else if(Character.isDigit (token.charAt (0))){
            return Lexeme.Number;
        }
        return Lexeme.Unknown;
    }

    Lexeme getLexeme(){
        return this.lexeme;
    }

    String getValue(){
        return this.value;
    }
}
