package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x==null || y==null){
            throw new IllegalArgumentException ();
        }
        if(x.isEmpty ()){
            return true;
        }

        if(x.size ()>y.size ()){
            return false;
        }

        List xList = new LinkedList (x);
        List yList = new LinkedList (y);
        Object xItem;
        Object yItem;
        xItem = xList.remove(0);
        while(!yList.isEmpty())
        {
            yItem = yList.remove(0);
            if(yItem.equals(xItem)){
                if(xList.isEmpty()) return true;
                xItem = xList.remove(0);
            }
        }
        return false;
    }
}
